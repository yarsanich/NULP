# CS-35 Computer networking lab task

## For running project

```
python manage.py makemigrations
python manage.py migrate # Migrate DB
python manage.py createsuperuser # Create user
python manage.py runserver # Run dev server
```

Test available at /education/themes
